package kz.aitu.javaSql.spring.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;

    @Column
    private String name;
    private String phone;
    private int groupid;

    public Student(int id, String name, String phone, int groupid) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.groupid = groupid;
    }

    public Student(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    @Override
    public String toString() {
        String groupNameToOutput;
        switch(groupid){
            case 1:
                groupNameToOutput = "CS-1901";
                break;
            case 2:
                groupNameToOutput = "CS-1902";
                break;
            case 3:
                groupNameToOutput = "CS-1903";
                break;
            case 4:
                groupNameToOutput = "CS-1904";
                break;
            case 5:
                groupNameToOutput = "CS-1905";
                break;
            default:
                groupNameToOutput = "unknown";
        }
        return String.format("Student id is " + id + ", name is " + name + ", phone is " + phone + ", group is "
                + groupNameToOutput);
    }
}