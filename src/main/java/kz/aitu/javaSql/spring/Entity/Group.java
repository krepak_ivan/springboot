package kz.aitu.javaSql.spring.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "university_group")
public class Group {
    @Id
    private long id;

    @Column
    private String name;

    public Group(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Group(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Group is " + name + ", group id is " + id);
    }
}