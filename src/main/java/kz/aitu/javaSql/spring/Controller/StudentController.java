package kz.aitu.javaSql.spring.Controller;

import kz.aitu.javaSql.spring.Entity.Student;
import kz.aitu.javaSql.spring.Repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class StudentController {
    private final StudentRepository studentRepository;
    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/students")
    public ResponseEntity<?> getStudents() {
        List<Student> studentProperties = studentRepository.findAllByGroupId();
        return ResponseEntity.ok(studentProperties.toString());
    }

    @GetMapping("students_2")
    public String getStudentsAsString(){
        String stringToOutput = "";
        List<Student> studentList = studentRepository.findAllByGroupId();
        for(int i = 0; i<studentList.size(); i++){
            stringToOutput = stringToOutput + studentList.get(i) + System.lineSeparator();
        }
        return stringToOutput;
    }
}