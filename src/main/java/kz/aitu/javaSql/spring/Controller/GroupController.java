package kz.aitu.javaSql.spring.Controller;

import kz.aitu.javaSql.spring.Entity.Group;
import kz.aitu.javaSql.spring.Repository.GroupRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class GroupController {
    private final GroupRepository groupRepository;
    public GroupController(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }
    @GetMapping("/groups")
    public ResponseEntity<?> getGroups() {
        List<Group> groupProperties = groupRepository.findAllByGroupId();
        return ResponseEntity.ok(groupProperties.toString());
    }

    @GetMapping("groups_2")
    public String getGroupsAsString(){
        String stringGroupToOutput = "";
        List<Group> groupList = groupRepository.findAllByGroupId();
        for(int i = 0; i < groupList.size(); i++){
            stringGroupToOutput = stringGroupToOutput + groupList.get(i) + System.lineSeparator();
        }
        return stringGroupToOutput;
    }
}