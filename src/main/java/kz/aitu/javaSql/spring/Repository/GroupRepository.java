package kz.aitu.javaSql.spring.Repository;

import kz.aitu.javaSql.spring.Entity.Group;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {
    @Query(value = "select * from university_group", nativeQuery = true)
    List<Group> findAllByGroupId();
}