package kz.aitu.javaSql.spring.Repository;

import kz.aitu.javaSql.spring.Entity.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    @Query(value = "select * from student", nativeQuery = true)
    List<Student> findAllByGroupId();
}